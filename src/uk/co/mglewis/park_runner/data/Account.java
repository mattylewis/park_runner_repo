package uk.co.mglewis.park_runner.data;

/** 
 * Container class for holding database data relating to course information
 */
public class Account {

	private final String LOG_TAG = "Park_Runner_Account";

	private int accountID;	
	private String accountName;
	private int numberOfRuns;
    private int mostRecentRunDate;
    private int bestTime;
    
	public Account(int accountID, String accountName, int numberOfRuns, int mostRecentRunDate, int bestTime)
    {
    	this.accountID = accountID;
    	this.accountName = accountName;
    	this.numberOfRuns = numberOfRuns;
    	this.mostRecentRunDate = mostRecentRunDate;
    	this.bestTime = bestTime;
    }
    
	public int getAccountID() {
		return accountID;
	}
	
	public String getAccountName() {
		return accountName;
	}
	
	public int getNumberOfRuns() {
		return numberOfRuns;
	}

	public int getMostRecentRunDate() {
		return mostRecentRunDate;
	}

	public String getMostRecentRunDateAsString() {
		String d = Integer.toString(mostRecentRunDate);
		String year = d.substring(0,4);
		String month = d.substring(4,6);
		String day = d.substring(6,8);
		
		return day + "/" + month + "/" + year;
	}
	
	public int getBestTime() {
		return bestTime;
	}
	
    public String getBestTimeAsString() {
    	String t = Integer.toString(bestTime);
    	String minutes = t.substring(0, t.length()-2); 
    	String seconds = t.substring(t.length()-2);
    	return minutes + ":" + seconds;
    }	
}
