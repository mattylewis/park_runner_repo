package com.shyyko.zxing.quick;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;
import com.shyyko.zxing.quick.camera.CameraManager;
import com.shyyko.zxing.quick.camera.CaptureHandler;
import com.shyyko.zxing.quick.camera.PreviewCallback;
import com.shyyko.zxing.quick.camera.view.BoundingView;
import com.shyyko.zxing.quick.camera.view.CameraPreviewView;

import uk.co.mglewis.park_runner.Launchpad;
import uk.co.mglewis.park_runner.R;

/**
 * Capture activity (camera barcode activity)
 */
public class CaptureActivity extends Activity {
    /**
     * Camera preview view
     */
    private CameraPreviewView cameraPreview;
    /**
     * Camera manager
     */
    private CameraManager cameraManager;
    /**
     * Capture handler
     */
    private Handler captureHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capture);

        // Create an instance of Camera
        cameraManager = new CameraManager();
        captureHandler = new CaptureHandler(cameraManager, this, new OnDecoded());
        //requesting next frame for decoding
        cameraManager.requestNextFrame(new PreviewCallback(captureHandler, cameraManager));

        // Create our Preview view and set it as the content of our activity.
        cameraPreview = (CameraPreviewView) findViewById(R.id.camera_preview);
        cameraPreview.setCameraManager(cameraManager);
        ((BoundingView) findViewById(R.id.bounding_view)).setCameraManager(cameraManager);
    }

    @Override
    protected void onPause() {
        super.onPause();
        cameraManager.release();
    }

    private class OnDecoded implements CaptureHandler.OnDecodedCallback {
        @Override
        public void onDecoded(String decodedData) {
        	if ( decodedData.length() < 2 ) {
        		return;
        	}

        	// trim the runnerID to remove the anticipated A
        	String runnerID = decodedData.substring(1);
        	
        	// add runnerID to saved IDs if a number
        	if ( isNumeric(runnerID) ) {
        		Toast.makeText(CaptureActivity.this, "ID Scanned", Toast.LENGTH_SHORT).show();
        		Launchpad.addIDToSavedIDs(runnerID);
        		
    			// request for a refresh of data from the parkrun site
    			Launchpad.setDataRequestRequired();
        		finish();
        	} else {
        		Toast.makeText(CaptureActivity.this, "Invalid ID", Toast.LENGTH_SHORT).show();
        	}
        }
        
        /**
         * Checks to see if the given string is a number (useful for validating barcode ids)
         */
        private boolean isNumeric(String str)  
        {  
          try  {  
            Integer.parseInt(str);  
          } catch(NumberFormatException nfe) {  
            return false;  
          }  
          return true;  
        }
    }
}
