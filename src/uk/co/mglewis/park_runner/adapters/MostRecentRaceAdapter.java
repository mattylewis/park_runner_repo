package uk.co.mglewis.park_runner.adapters;

import java.util.ArrayList;

import uk.co.mglewis.park_runner.Launchpad;
import uk.co.mglewis.park_runner.R;
import uk.co.mglewis.park_runner.data.DatabaseHandler;
import uk.co.mglewis.park_runner.data.Result;
 
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
 
/**
 * Adapter class that provides data for the ListView on the RecentCourseResults
 * page fragment
 */
public class MostRecentRaceAdapter extends BaseAdapter {
 
    private Activity activity;
    private ArrayList<Result> recentCourseResultData;
    private static LayoutInflater inflater = null;

    /**
     * Constructor. Connects to the database and extracts the most recent course results.
     * 
     * Two database queries are required in this method: 
     * - The first query determines the most recent run that runner completed
     * - The second query returns a list of all runners who completed that run 
     * 	 (the CourseResultURL is used as part of the where clause)
     * 
     * @param a the activity that this adapter is called from. Used to inflate the layout
     */
    public MostRecentRaceAdapter(Activity a) {
        activity = a;
        DatabaseHandler dh = new DatabaseHandler(a.getBaseContext());
        
        // get the id of the most recent race the runner completed an
        ArrayList<Result> mostRecentResultList = dh.getResults(
        		" SELECT * FROM " + DatabaseHandler.TABLE_RESULTS + 
        		" WHERE " + DatabaseHandler.KEY_RUNNER_ID + " = " + Launchpad.getRID() + 
        		" ORDER BY " + DatabaseHandler.KEY_COURSE_DATE + " DESC " +  
        		" LIMIT 1");
        String cURL = mostRecentResultList.get(0).getCourseResultURL();
        
        recentCourseResultData = dh.getResults(
        		" SELECT * FROM " + DatabaseHandler.TABLE_RESULTS + 
        		" WHERE " + DatabaseHandler.KEY_COURSE_RESULT_URL + " = '" + cURL + "' " +
        		" ORDER BY " + DatabaseHandler.KEY_RUNNER_POSITION + " ASC ");
         
        dh.close();
        
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
 
    /**
     * Returns the number of runners in the most recent course result
     */
    public int getCount() {
        return recentCourseResultData.size();
    }
 
    /**
     * Returns the position in the list. Required for implementing BaseAdaptor
     * 
     * This can be useful if implementing different formatting for odd/even rows.
     */
    public Object getItem(int position) {
        return position;
    }
 
    /**
     * Returns the position in the list. Required for implementing BaseAdaptor
     * 
     * This can be useful if implementing different formatting for odd/even rows.
     */
    public long getItemId(int position) {
        return position;
    }
 
    /**
     * Populates a row in the list. Contents of the row is determined by the position variable.
     * 
     * @param position the position in the list, code uses this variable show the runner who finished in this position 
     * @param convertView the row view to update
     * @param parent the parent view that the convertView inherits from
     * @return the inflated and populated list row
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        
        if (convertView == null) {
            view = inflater.inflate(R.layout.list_row_race_result, null);
        }
        
        TextView rName = (TextView)view.findViewById(R.id.pr_list_row_name);
        TextView rPosition = (TextView)view.findViewById(R.id.pr_list_row_position); 
        TextView rTime = (TextView)view.findViewById(R.id.pr_list_row_time); 
        TextView rAgeGrade = (TextView)view.findViewById(R.id.pr_list_row_age_grade);
        
        Result r = recentCourseResultData.get(position);
        rName.setText(r.getRunnerName());
        rPosition.setText("Position: " + r.getRunnerPositionAsString());
        rAgeGrade.setText("Age Grade: " + r.getRunnerAgeGrade());
        
        if (r.getRunnerName().equals("Unknown")) {
        	rTime.setText("N/A");
        	rTime.setTextColor(view.getResources().getColor(R.color.gray_text));
        } else {
        	rTime.setText(r.getRunnerTimeAsString());
        	rTime.setTextColor(view.getResources().getColor(R.color.black_text));
        }
        return view;
    }
    
    /** 
     * Return the title text for the fragment
     * 
     * This should be in the form "RACE_NAME" + "#" + "COURSE_NAME"
	**/
    public String getTitleText() {
        DatabaseHandler dh = new DatabaseHandler(activity);
        Result titleDetails = dh.getResults(
        		" SELECT * FROM " + DatabaseHandler.TABLE_RESULTS + 
        		" WHERE " + DatabaseHandler.KEY_RUNNER_ID + " = " + Launchpad.getRID() + 
        		" ORDER BY " + DatabaseHandler.KEY_COURSE_DATE + " DESC " +  
        		" LIMIT 1").get(0);        
        dh.close();
        return titleDetails.getCourseName() + " #" + titleDetails.getCourseNumber();
    }
}