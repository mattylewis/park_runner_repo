package uk.co.mglewis.park_runner.adapters;

import java.util.ArrayList;

import uk.co.mglewis.park_runner.Launchpad;
import uk.co.mglewis.park_runner.R;
import uk.co.mglewis.park_runner.data.DatabaseHandler;
import uk.co.mglewis.park_runner.data.Result;
 
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
 
/**
 * Adapter class that provides data to list of all results achieved by
 * a athlete
 */
public class AllRunnerResultsAdapter extends BaseAdapter {
 
    private Activity activity;
    private ArrayList<Result> recentResultData;
    private static LayoutInflater inflater = null;

    /**
     * Constructor that:
     *  - inflates layout 
     *  - queries the database for all runs completed by the logged in runner
     *  
     * @param a parent activity that adapter is created from
     */
    public AllRunnerResultsAdapter(Activity a) {
        activity = a;
        
        DatabaseHandler dh = new DatabaseHandler(activity.getBaseContext());
        recentResultData = dh.getResults(
        		" SELECT * FROM " + DatabaseHandler.TABLE_RESULTS + 
        		" WHERE " + DatabaseHandler.KEY_RUNNER_ID + " = " + Launchpad.getRID() + " " +
        		" ORDER BY " + DatabaseHandler.KEY_COURSE_DATE + " DESC");
        dh.close();
        
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
 
    /**
     * Returns the number of runs the runner has completed
     */
    public int getCount() {
        return recentResultData.size();
    }
 
    /**
     * Returns the position in the list. Required for implementing BaseAdaptor
     * 
     * This can be useful if implementing different formatting for odd/even rows.
     */
    public Object getItem(int position) {
        return position;
    }
 
    /**
     * Returns the position in the list. Required for implementing BaseAdaptor
     * 
     * This can be useful if implementing different formatting for odd/even rows.
     */
    public long getItemId(int position) {
        return position;
    }
 
    /**
     * Populates a row in the list. Contents of the row is determined by the position variable.
     * 
     * @param position the position in the list, code uses this variable show the runner's nth run (P1 = most recent)
     * @param convertView the row view to update
     * @param parent the parent view that the convertView inherits from
     * @return the inflated and populated list row
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        
        if (convertView == null) {
            view = inflater.inflate(R.layout.list_row_runner_result, null);
        }
        
        TextView rCourse = (TextView)view.findViewById(R.id.pr_list_row_course);
        TextView rDate = (TextView)view.findViewById(R.id.pr_list_row_date); 
        TextView rTime = (TextView)view.findViewById(R.id.pr_list_row_time); 
        TextView rPosition = (TextView)view.findViewById(R.id.pr_list_row_position);
        
        // Setting all values in listview
        Result r = recentResultData.get(position);
        
        rCourse.setText(r.getCourseName());
        rDate.setText("Date: " + r.getCourseDateAsString());
        rTime.setText(r.getRunnerTimeAsString());
        rPosition.setText("Position: " + r.getRunnerPosition());
        
        return view;
    }
}