package uk.co.mglewis.park_runner;

import uk.co.mglewis.park_runner.R;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

public class IDEntry extends Activity {
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.enter_id);
		EditText idInput = (EditText) this.findViewById(R.id.input_athelete_number);
		idInput.setOnEditorActionListener(new IDEntryActionListener());
    }
	
	@Override
	protected void onResume() {
		super.onResume();

		EditText idInput = (EditText) this.findViewById(R.id.input_athelete_number);
		idInput.setFocusableInTouchMode(true);
		idInput.requestFocus();
		final InputMethodManager inputMethodManager = (InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.showSoftInput(idInput, InputMethodManager.SHOW_IMPLICIT);
	}
    
	private class IDEntryActionListener implements OnEditorActionListener {
		
		public static final String LOG_TAG = "IDEntryTextWatcher";
		
		@Override
		public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		    if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE || event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
		        	String inputID = v.getText().toString();
		    		Log.d(LOG_TAG, "IDEntry, recieved input: " + inputID);
		    		
		    		if ( !Launchpad.savedIDsContainsID(inputID) ) {
		    			// update the saved ID list
		    			Log.d(LOG_TAG, "Adding " + inputID + " to saved ID list...");
		    			Launchpad.addIDToSavedIDs(inputID);
		    			
		    			// request for a refresh of data from the parkrun site
		    			Launchpad.setDataRequestRequired();
		    		} else {
		    			Log.d(LOG_TAG, inputID + " has already been added to saved ID list");
		    			Toast.makeText(getBaseContext(), inputID + " has already been saved.", Toast.LENGTH_SHORT).show();
		    		}
		    		finish();
		        	return true;
		    }
		    return false; 
		}
	}
}
