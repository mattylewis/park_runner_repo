package uk.co.mglewis.park_runner;

import uk.co.mglewis.park_runner.fragments.MostRecentRace;
import uk.co.mglewis.park_runner.fragments.RunnerSummary;
import uk.co.mglewis.park_runner.fragments.AllRunnerResults;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

/**
 * This activity is the container for the users session after they log in.
 * User information is split into fragments that can be accessed by swiping 
 * to the right and left.
 * 
 * Currently implemented fragment include
 * 	- General parkrun aggregated data (page showing best time, number of runs etc)
 *  - List of all parkrun results
 *  - List of most recent run results
 *  - (TBC) Graph showing result progression over time
 * 
 */
public class SlidingActivityContainer extends FragmentActivity {

	private final String LOG_TAG = "Park_Runner_Slide_Container";
	
    /**
     * The number of page fragments to show 
     */
    private static final int NUM_PAGES = 3;

    /**
     * The pager widget handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;
 
    /**
     * Creates the new Activity. This should be called after passing through the login screen.
     * 
     * Method instantiates child fragments that can then be swiped through.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sliding_activity_container);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        
        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
            	invalidateOptionsMenu();
            }
        });
    }

    /**
     * Creates the options menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.activity_screen_slide, menu);
        
        // auto login check box needs to be ticked if auto login is enabled
        MenuItem item = menu.findItem(R.id.action_automatic_log_in);
        if (Launchpad.getAutoLogin() && Launchpad.getRID() == Launchpad.getAutoLoginID()) {
        	item.setChecked(true);        	
        }
        
        return true;
    }

    /**
     * Method called when option is selected from the menu.
     * 
     * Switch statement determines what action to take
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
            case R.id.action_automatic_log_in :
            	boolean checked = Launchpad.getAutoLogin();
            	if ( checked == true ) {
                	item.setChecked(false);
                	Launchpad.setAutoLogin(false);
            	} else {
            		item.setChecked(true);
                	Launchpad.setAutoLogin(true);
                	Launchpad.setAutoLoginID();
            	}
            	return true;
                        	
            case R.id.action_exit :
            	finish();
            	return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A simple pager adapter that represents the child fragments
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        
    	public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

    	/**
    	 * Method to determine what fragment should be loaded at what position.
    	 * 
    	 * As new fragments are written, they can be added here
    	 */
        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
            	return RunnerSummary.create(position);
            }
            else if (position == 1) {
				return MostRecentRace.create(position);
			}
            else {
            	return AllRunnerResults.create(position);
            }
        }

        /**
         * Returns the name of the given page
         */
        @Override
        public CharSequence getPageTitle (int position) {
            if (position == 0) {
            	return "Runner Summary";
            }
            else if (position == 1) {
				return "Most Recent Race Result";
			}
            else {
            	return "Results History";
            }
        }
        
        /** 
         * Returns the number of child fragments
         */
        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}
