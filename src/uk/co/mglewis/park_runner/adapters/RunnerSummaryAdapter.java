package uk.co.mglewis.park_runner.adapters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.Activity;

import uk.co.mglewis.park_runner.Launchpad;
import uk.co.mglewis.park_runner.data.Course;
import uk.co.mglewis.park_runner.data.DatabaseHandler;
import uk.co.mglewis.park_runner.data.Result;

/**
 * Adaptor for runner summary fragment.
 * 
 * Note: unlike AllRunnerResultsAdapter and MostRecentRaceAdapter classes this 
 * class does not extend BaseAdapter. Instead it provides a number of utility 
 * methods that can be used for retrieving data from the database. 
 * 
 */
public class RunnerSummaryAdapter {

	private List<Result> recentTimes = null;
	private List<Result> bestTimes = null;
	private List<Course> favouriteCourses = null;
	
	private int numberOfRuns;
	private int daysSinceLastRun;
	
	/**
	 * Run all of the sql queries that can then be retrieved using the getters
	 */
	public RunnerSummaryAdapter(Activity a) {
		String query = null;
		DatabaseHandler dh = new DatabaseHandler(a);
        
		query = " SELECT * FROM " + DatabaseHandler.TABLE_RESULTS + 
        		" WHERE " + DatabaseHandler.KEY_RUNNER_ID + " = " + Launchpad.getRID() +
        		" ORDER BY " + DatabaseHandler.KEY_COURSE_DATE + " DESC LIMIT 3 ";
				
        recentTimes = dh.getResults(query);
        
        query = " SELECT * FROM " + DatabaseHandler.TABLE_RESULTS + 
        		" WHERE " + DatabaseHandler.KEY_RUNNER_ID + " = " + Launchpad.getRID() +
        		" ORDER BY " + DatabaseHandler.KEY_RUNNER_TIME + " ASC LIMIT 3";
        
        bestTimes = dh.getResults(query);
        
        query = " SELECT " + DatabaseHandler.KEY_COURSE_NAME + ", " +
        					"COUNT( " + DatabaseHandler.KEY_COURSE_NAME + " ), " +
        					"MIN( " + DatabaseHandler.KEY_RUNNER_POSITION + " ), " +
        					"MIN( " + DatabaseHandler.KEY_RUNNER_TIME + " ), " +
        					"MAX( " + DatabaseHandler.KEY_COURSE_DATE + " ) " + 
        		" FROM " + DatabaseHandler.TABLE_RESULTS + 
        		" WHERE " + DatabaseHandler.KEY_RUNNER_ID + " = " + Launchpad.getRID() +
        		" GROUP BY " + DatabaseHandler.KEY_COURSE_NAME + 
        		" ORDER BY " + "COUNT( " + DatabaseHandler.KEY_COURSE_NAME + " ) DESC LIMIT 3";
        		
        favouriteCourses = dh.getCourses(query);
        
        query = " SELECT * FROM " + DatabaseHandler.TABLE_RESULTS + 
        		" WHERE " + DatabaseHandler.KEY_RUNNER_ID + " = " + Launchpad.getRID();

        numberOfRuns = dh.getResults(query).size();
        
        query = " SELECT * FROM " + DatabaseHandler.TABLE_RESULTS + 
        		" WHERE " + DatabaseHandler.KEY_RUNNER_ID + " = " + Launchpad.getRID() + 
        		" ORDER BY " + DatabaseHandler.KEY_COURSE_DATE + " DESC LIMIT 1";

        List<Result> mostRecentResults = dh.getResults(query);
        String lastRunString = "";
        if ( mostRecentResults.size() >= 1 ) {
        	lastRunString = dh.getResults(query).get(0).getCourseDateAsString();
        }
        
        try {
			 SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
			 Date lastRunDate = simpleDateFormat.parse(lastRunString);
			 Date currentDate = new Date();
			 daysSinceLastRun = (int)((currentDate.getTime() - lastRunDate.getTime()) / (1000 * 60 * 60 * 24));
		} catch (ParseException e) {
			daysSinceLastRun = 0;
		}
        
        dh.close();
	}
	
	public List<Result> getRecentTimes() {
		return recentTimes;
	}
	
	public List<Result> getBestTimes() {
		return bestTimes;
	}
	
	public List<Course> getFavouriteCourses() {
		return favouriteCourses;
	}

	/**
	 * @return total number of runs in the format "[1 parkrun/2 parkruns] completed."
	 */
	public String getNumberOfRunsAsString() {
		if ( numberOfRuns == 1 ) {
			return "1 parkrun completed.";
		}
		return Integer.toString(numberOfRuns) + " parkruns completed.";
	}
	
	/**
	 * @return days since last run in the format "Last run [1 day"/"2 days] ago."
	 */
	public String getDaysSinceLastRunAsString() {
		if ( daysSinceLastRun == 1 ) {
			return "Last run 1 day ago.";
		}
		return "Last run " + Integer.toString(daysSinceLastRun) + " days ago.";
	}
	
}

