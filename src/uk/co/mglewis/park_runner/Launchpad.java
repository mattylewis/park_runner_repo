package uk.co.mglewis.park_runner;

import java.util.HashSet;
import java.util.Set;

import com.shyyko.zxing.quick.CaptureActivity;

import uk.co.mglewis.park_runner.adapters.LaunchpadAdaptor;
import uk.co.mglewis.park_runner.data.DatabaseHandler;
import uk.co.mglewis.park_runner.parser.Parser;
import uk.co.mglewis.park_runner.R;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

/**
 * The launchpad activity for the application. 
 * 
 * This activity starts the log in screen process
 */
public class Launchpad extends FragmentActivity {
	
	private final static String LOG_TAG = "Launchpad";

	// variable to denote which is the active runner ID
	private static String rID;
	
	// variable to denote when upon refreshing the launchpad screen, new data should be downloaded from the parkrun site
	private static boolean dataRequest = false;
	
	private static SharedPreferences settings = null;
	public static final String PREFS_FILE = "park_run_prefs";
	public static final String PREFS_KEY_AUTO_LOGIN_ENABLED = "auto_login_enabled";
	public static final String PREFS_KEY_AUTO_LOGIN_ID = "auto_login_id";
	public static final String PREFS_KEY_SAVED_ACCOUNTS = "saved_accounts";
	
	/**
	 * Entry point for the application. 
	 *
	 * Launches the start screen with log on options
	 */
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.launchpad);
		settings = getSharedPreferences(PREFS_FILE, 0);
		
        // start downloading data for all saved accounts in the background
		FetchResultsAsync resultsFetcher = new FetchResultsAsync(this);
		resultsFetcher.execute();
		
		// if auto login is enabled go straight to a users summary page
		if (getAutoLogin() == true && savedIDsContainsID(getAutoLoginID())) {
			setRID(getAutoLoginID());
			try {
				startActivity(new Intent(Launchpad.this, SlidingActivityContainer.class));
			} catch (Exception e) {
				Log.e(LOG_TAG, "Failed to start activity container");
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Runs on activity resume. 
	 * 
	 * This is where the screen should be refreshed to show the most up to date information.
	 * 
	 * Method flow:
	 * 	- Get the GUI element representing the list of saved users
	 * 	- Get the list of of saved users from shared preferences
	 */
	@Override
	public void onResume() {
		super.onResume();
		
		// refresh the gui user list
		refreshUserList();
		
        // start downloading data for all saved accounts in the background
		if ( getDataRequestStatus() == true ) {
			FetchResultsAsync resultsFetcher = new FetchResultsAsync(this);
			resultsFetcher.execute();
		}
	}
	
    /**
     * Creates the options menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.launchpad, menu);
        return true;
    }
	
    /**
     * Method called when option is selected from the menu.
     * 
     * Switch statement determines what action to take
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	
    	switch (item.getItemId()) {
                            
            case R.id.action_refresh_data :
        		FetchResultsAsync resultsFetcher = new FetchResultsAsync(this);
        		resultsFetcher.execute();
            	return true;
            
            case R.id.clear_saved_users :
            	// remove saved user data
            	clearSavedIDs();
    			
    			// truncate database table
    			DatabaseHandler dh = new DatabaseHandler(getBaseContext());
    			dh.truncate();
    			dh.close();

    			refreshUserList();
            	return true;
            	
            case R.id.action_exit :
            	// not yet implemented
            	return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    /**
     * Refreshes the saved accounts list (on the gui)
     * 
     * Method pulls out saved users from settings container (shared prefs)
     */
	private void refreshUserList()
	{
		Set<String> savedIDs = getSavedIDs();
	    View savedUsersContainer = (LinearLayout) this.findViewById(R.id.container_saved_users);
	    
	    if ( savedIDs.size() == 0 ) {
	    	Log.d(LOG_TAG, "No saved users in shared preferences file, hiding saved user list on launchpad");
	    	savedUsersContainer.setVisibility(View.INVISIBLE);
	    } else {
	    	Log.d(LOG_TAG, "Refreshing " + savedIDs.size() + " saved users, adding details to launchpad");
	    	savedUsersContainer.setVisibility(View.VISIBLE);
	    	
	    	LinearLayout l = (LinearLayout) this.findViewById(R.id.list_saved_users);
	    	l.removeAllViews();

	    	LaunchpadAdaptor a = new LaunchpadAdaptor(this, savedIDs);
			
		    for (int i = 0; i < a.getCount(); i++) {
		    	View item = a.getView(i, null, null);
		    	final int runnerID = a.getRunnerID(i);

		    	if ( item != null ) {
		        	l.addView(item);
		        	Log.d(LOG_TAG, "Added user to saved user list:" + runnerID);
		        	
		        	item.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							rID = Integer.toString(runnerID);
							try {
								startActivity(new Intent(Launchpad.this, SlidingActivityContainer.class));
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					});
		        } else {
		    		l.addView(a.getLoadingView());
		    		Log.d(LOG_TAG, "Added loading placeholder to saved user list for id: " + runnerID);
		    	}
		    }	    	
	    }
	}
	
	/**
	 * Launch the scan barcode component. Currently just logs in without the online download step
	 * 
	 * @param v the current view, used for retrieving the athlete ID
	 */
	public void onScanBarcodeButtonPress(View v) {
		try {
			startActivity(new Intent(Launchpad.this, CaptureActivity.class));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void onEnterIDButtonPress(View v) {
		try {
			startActivityForResult(new Intent(Launchpad.this, IDEntry.class), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Private class for fetching ParkRun data from the web site
	 */
	private class FetchResultsAsync extends AsyncTask<Void, Void, Void> {

		private Context mainContext;
		
		/**
		 * Basic constructor. Required to access application context.
		 * 
		 * @param baseContext the application's context at run time
		 */
		public FetchResultsAsync(Context baseContext) {
			mainContext = baseContext;
		}

		@Override
		/**
		 * Pre execution tasks prior to updating user data
		 * - make the log in spinner visible in the action bar
		 * 
		 */
		protected void onPreExecute() {
			Log.i(LOG_TAG, "Attempting to fetch data from the parkrun website");
			setProgressBarIndeterminateVisibility(true);
			setDataRequestWorked();
		}
		
		/**
		 * Download the provided athelete's results from the web site.
		 * 
		 * This method calls two functions, this is to retrieve all of the runners 
		 * individual results and the results of everyone who ran his most recent run with.
		 * 
		 * Total loading time should not be too bad as only the following has to be loaded:
		 *  - 1 summary page showing the runners most recent results (overall summary page)
		 *  - 1 page per course the runner has ever raced at (course summary page)
		 *  - 1 results page for the most recent run that the runner completed
		 * 
		 * @param atheleteNumber the runnerID of the athlete (as it appears on their barcode)
		 */
		@Override
		protected Void doInBackground(Void... v) {
			Set<String> savedIDs = getSavedIDs();
						
			if ( savedIDs.size() != 0 )
			{
				Parser parser = new Parser(mainContext, savedIDs);
				parser.checkForNewResults();
			}
			return null;
		}

		/**
		 * After the data collection job has been run:
		 * - re-hide the loading bar (necessary in case the back button is pressed)
		 * - Open the SlidingActivityContainer activity
		 * - Provide a notification that all data did not download as planned
		 */
		@Override
		protected void onPostExecute(Void vd) {
			Log.i(LOG_TAG, "Data successfully downloaded from the parkrun site");
			setProgressBarIndeterminateVisibility(false);
			refreshUserList();
		}
	}   
	
	/**
	 * Requests for new data to be updated from the parkrun website on the next time the launchpad:
	 * - resumes
	 * - calls the refreshUserList() method
	 */
	public static void setDataRequestRequired() {
		dataRequest = true;
	}
	
	/**
	 * Indicates that a request for new data from the parkrun website is in progress
	 */
	private static void setDataRequestWorked() {
		dataRequest = false;
	}
	
	/**
	 * @return Returns the status of dataRequest
	 */
	public static boolean getDataRequestStatus() {
		return dataRequest;
	}
	
	/**
	 * Setter for rID
	 */
	public static void setRID(String runnerID) {
		rID = runnerID;
	}
	
	/**
	 * Getter for rID
	 */
	public static String getRID() {
		return rID;
	}
	
	/**
	 * @return the set of savedIDs in the shared prefs
	 */
	public static Set<String> getSavedIDs() {
		return settings.getStringSet(PREFS_KEY_SAVED_ACCOUNTS, new HashSet<String>());
	}
	
	/**
	 * Deletes the saved accounts shared preference object
	 */
	public static void clearSavedIDs() {
		Editor editor = Launchpad.settings.edit();
		editor.remove(Launchpad.PREFS_KEY_SAVED_ACCOUNTS);
		editor.remove(Launchpad.PREFS_KEY_AUTO_LOGIN_ID);
		editor.apply();
	}

	/**
	 * @return true if the saved IDs object contains a given runner ID
	 */
	public static boolean savedIDsContainsID(String runnerID) {
		return getSavedIDs().contains(runnerID);
	}
	
	/**
	 * Add an ID the the saved accounts shared preferences object
	 */
	public static void addIDToSavedIDs(String runnerID) {
		Log.i(LOG_TAG, "Adding " + runnerID + " to saved IDs");		
		Set<String> savedIDs = getSavedIDs();
		
		Editor editor = Launchpad.settings.edit();
		editor.remove(Launchpad.PREFS_KEY_SAVED_ACCOUNTS);
		editor.apply();
		
		savedIDs.add(runnerID);
		editor.putStringSet(Launchpad.PREFS_KEY_SAVED_ACCOUNTS, savedIDs);
		editor.apply();
	}
	
	/**
	 * Removes an ID from the saved IDs list
	 */
	public static void removeIDFromSavedIDs(String runnerID) {
		Log.i(LOG_TAG, "Removing " + runnerID + " from saved IDs");
		Set<String> savedIDs = getSavedIDs();
		clearSavedIDs();
		savedIDs.remove(runnerID);
		
		Editor editor = Launchpad.settings.edit();
		editor.putStringSet(Launchpad.PREFS_KEY_SAVED_ACCOUNTS, savedIDs);
		editor.apply();
	}
	
	/**
	 * getter for auto login flag
	 */
	public static boolean getAutoLogin() {
		return Launchpad.settings.getBoolean(Launchpad.PREFS_KEY_AUTO_LOGIN_ENABLED, false);
	}
	
	/**
	 * setter for auto login - true indicates checked
	 */
	public static void setAutoLogin(boolean status) {
		Editor editor = Launchpad.settings.edit();
		editor.putBoolean(Launchpad.PREFS_KEY_AUTO_LOGIN_ENABLED, status);
		editor.apply();
	}
	
	/**
	 * setter for the auto login runner id
	 * @param runnerID
	 */
	public static void setAutoLoginID() {
		Editor editor = Launchpad.settings.edit();
		editor.putString(Launchpad.PREFS_KEY_AUTO_LOGIN_ID, getRID());
		editor.apply();
	}
	
	/**
	 * getter for the auto login runner id
	 */
	public static String getAutoLoginID() {
		return settings.getString(PREFS_KEY_AUTO_LOGIN_ID, "-1");
	}
}