package uk.co.mglewis.park_runner.data;

import android.util.Log;

/** 
 * Container class for holding database data relating to the results table
 */
public class Result {

	private final String LOG_TAG = "Park_Runner_Result";
	
    private String runnerName;
    private int runnerID;
    private int runnerTime;
	private String runnerAgeCat;
    private int runnerPosition;
    private String runnerAgeGrade;
    private String runnerGender;
    private String courseName;
    private int courseDate;
    private int courseNumber;
    private String courseResultURL;
    
    public Result (String rName, int rID, int rTime, String rAgeCat, int rPositon, String rAgeGrade, 
    		String rGender, String cName, int cDate, int cNumber, String cResultURL) {
    	
    	runnerName = rName;
    	runnerID = rID;
    	runnerTime = rTime;
    	runnerAgeCat = rAgeCat;
    	runnerPosition = rPositon;
    	runnerAgeGrade = rAgeGrade;
    	runnerGender = rGender;
    	courseName = cName;
    	courseDate = cDate;
    	courseNumber = cNumber;
    	courseResultURL = cResultURL;
    }
    
	public String getRunnerName() {
		return runnerName;
	}

	public int getRunnerID() {
		return runnerID;
	}

    public int getRunnerTime() {
		return runnerTime;
	}

    public String getRunnerTimeAsString() {
    	String t = Integer.toString(runnerTime);
    	String minutes = t.substring(0, t.length()-2); 
    	String seconds = t.substring(t.length()-2);
    	return minutes + ":" + seconds;
    }
	
	public String getRunnerAgeCat() {
		return runnerAgeCat;
	}
	
	public int getRunnerPosition() {
		return runnerPosition;
	}
	
	public String getRunnerPositionAsString() {
		return Integer.toString(runnerPosition);
	}
	
	public String getRunnerAgeGrade() {
		return runnerAgeGrade;
	}

	public String getRunnerGender() {
		return runnerGender;
	}

	public String getCourseName() {
		return courseName;
	}

	public int getCourseDate() {
		return courseDate;
	}
	
	public String getCourseDateAsString() {
		String d = Integer.toString(courseDate);
		String year = d.substring(0,4);
		String month = d.substring(4,6);
		String day = d.substring(6,8);
		
		return day + "/" + month + "/" + year;
	}

	public int getCourseNumber() {
		return courseNumber;
	}
	
	public String getCourseResultURL() {
		return courseResultURL;
	}
	
	public void printResult() {
		Log.d(LOG_TAG,"------------------------------------------------");
		Log.d(LOG_TAG,"Runner Name: " + runnerName);
		Log.d(LOG_TAG,"Runner ID:" + runnerID);
		Log.d(LOG_TAG,"Runner Time: " + runnerTime);
		Log.d(LOG_TAG,"Runner Age Cat: " + runnerAgeCat);
		Log.d(LOG_TAG,"Runner Position: " + runnerPosition);
		Log.d(LOG_TAG,"Runner Age Grade: " + runnerAgeGrade);
		Log.d(LOG_TAG,"Runner Gender: " + runnerGender);
		Log.d(LOG_TAG,"Course Name: " + courseName);
		Log.d(LOG_TAG,"Course Date: " + courseDate);
		Log.d(LOG_TAG,"Course Number: " + courseNumber);
		Log.d(LOG_TAG,"Course Result URL: " + courseResultURL);
		Log.d(LOG_TAG,"------------------------------------------------");
	}
}
