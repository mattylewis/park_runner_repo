package uk.co.mglewis.park_runner.parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import uk.co.mglewis.park_runner.Launchpad;
import uk.co.mglewis.park_runner.data.DatabaseHandler;
import uk.co.mglewis.park_runner.data.Result;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

/**
 * Class that downloads data from the park run website and adds it to the 
 * applications database
 */
public class Parser {

	private final String LOG_TAG = "Park_Runner_Parser";
	private final Set<String> athleteIDs;
	private Context context;
	
	public Parser(Context context, Set<String> atheleteIDs) {
		this.context = context;
		this.athleteIDs = atheleteIDs;
	}

	/**
	 * Primary method in the parser class. Responsible for coordinating the get requests to 
	 * the parkrun website and adding rows to the database. This is achieved by calling the
	 * getRunnerCompleteResults and getMostRecentCourseResults methods.
	 * 
	 * Note: values in database must be updated after fetching runner complete results to
	 * ensure that the correct most recent course data is downloaded.
	 */
	public void checkForNewResults() {
		DatabaseHandler databaseHandler = new DatabaseHandler(context);
		databaseHandler.addResults(getAllRunnerResults());
		databaseHandler.addResults(getMostRecentRaceResults());
		databaseHandler.close();
	}	
	
	/**
	 * Connect to the runners summary page and loop through each course that the runner has completed.
	 * 
	 * Note, to improve performance a parallel for loop has been implemented in the code below.
	 */
	public List<Result> getAllRunnerResults() {
		ArrayList<Result> results = new ArrayList<Result>();
				
		try {
			// run the fetch operations for the runner summary pages concurrently
			ExecutorService service = Executors.newFixedThreadPool(athleteIDs.size());
			List<Future<Elements>> elementFutures = new ArrayList<Future<Elements>>();
			
			for ( final String athleteID : athleteIDs ) {
				Callable<Elements> callable = new Callable<Elements>() {
					public Elements call() throws IOException {
						Log.i(LOG_TAG, "Attempting connection to athlete main results history page for ID=" + athleteID);
						Document doc = Jsoup.connect("http://www.parkrun.org.uk/results/athleteresultshistory/?athleteNumber=" + athleteID).get();
						Elements courseLinks = doc.getElementsByAttributeValueContaining("href", "results/athletehistory?athleteNumber=" + athleteID);
						if ( courseLinks.size() == 0 ) {
							Log.w(LOG_TAG, "No run data for: " + athleteID + ". Removing from saved ID list");
							Launchpad.removeIDFromSavedIDs(athleteID);
							((Activity) context).runOnUiThread(new Runnable() { public void run() {
							            Toast.makeText((Activity) context, athleteID + " is not a valid Park Run ID", Toast.LENGTH_SHORT).show();
							}});
						}
						return courseLinks;
					}
				};
				elementFutures.add(service.submit(callable));
			}
			service.shutdown();
			
			// this combines all elements fetched concurrently into one big list
			Elements combinedElements = new Elements();
			for ( Future<Elements> elements : elementFutures ) {
				combinedElements.addAll(elements.get());
			}
						
			// the elements contain all the courses that each runner has raced at, go off and fetch all the runners runs
			// from each course
			// network requests are handled concurrently to improve performance. Create one new thread per element
			service = Executors.newFixedThreadPool(combinedElements.size());
		    List<Future<List<Result>>> futures = new ArrayList<Future<List<Result>>>();

		    for (final Element e : combinedElements) {
		        Callable<List<Result>> callable = new Callable<List<Result>>() {
		            public List<Result> call() {
		                return getRunnerCourseResults(e.attr("href"));
		            }
		        };
		        futures.add(service.submit(callable));
		    }
		    service.shutdown();

		    for (Future<List<Result>> future : futures) {
		        results.addAll(future.get());
		    }
		} 
		catch (Exception e) {
			Log.e("Results","Error fetching and formatting results data.");
		}
		
		return results;
	}
	
	/**
	 * Get all results for a single runner at a single course that they have participated in.
	 * 
	 * Method works by connecting to course summary page (see link below) and scraping off the page
	 * into result objects that can then be added into the database
	 * 
	 * @param url the url of the course summary page
	 * @param runnerID id of the runner to capture (not used within logic, just record to create result objects)
	 */
	public List<Result> getRunnerCourseResults(String url) {
		ArrayList<Result> results = new ArrayList<Result>();
		
		try {
			String runnerID = url.substring(url.lastIndexOf("=")+1);
			Log.i(LOG_TAG, "Attempting connection to runner's course summary page. ID=" + runnerID + " URL=" + url);
			Document doc = Jsoup.connect(url).get();
			
			// get the name of the runner
			String rName = "";
			String pageHeader = doc.select("h2").get(0).text();
			String[] rNames = pageHeader.substring(0, pageHeader.indexOf(" - ")).toLowerCase(Locale.US).split("\\s+");
			for ( String n : rNames ) {
				rName = (rName + " " + n.substring(0, 1).toUpperCase(Locale.US) + n.substring(1)).trim();
			}

			int rID = Integer.parseInt(runnerID);
			String rAgeCat = (doc.getElementsContainingOwnText("Most recently ran in ").get(0).text());
			rAgeCat = rAgeCat.substring(rAgeCat.indexOf("Most recently ran in ")+21).replaceAll("( [^ ]+){2}$", "");
			String cName = pageHeader.substring(pageHeader.indexOf(" at ")+4).replaceAll("( [^ ]+){3}$", "");
			
			// first get all the tables that the results could be in
			Element resultTable = doc.getElementsByAttributeValueMatching("id", "results").get(2);
			
			// then add everything to a result object that can then be added into the database
			for (Element tr : resultTable.select("tr")) {
				Elements tds = tr.select("td");
				
				if ( tds.size() > 3  ) {
					int rPosition = Integer.parseInt(tds.get(2).text()); 
					int rTime = Integer.parseInt(((tds.get(3).text().split(":"))[0])+((tds.get(3).text().split(":"))[1]));
					String rAgeGrade = tds.get(4).text().replaceAll("\\s+","");
					String rGender = rAgeCat.substring(1, 2);
					int cDate = Integer.parseInt(tds.get(0).text().substring(6)+tds.get(0).text().substring(3,5)+tds.get(0).text().substring(0,2));
					int cNumber = Integer.parseInt(tds.get(1).text());
					String cURL = tds.get(1).select("a").get(0).attr("href");
					
					Result r = new Result(rName, rID, rTime, rAgeCat, rPosition, rAgeGrade, rGender, cName, cDate, cNumber, cURL);
					results.add(r);
				}
			}	
		} catch (Exception e) {
			Log.e("Results","Error in getRunnerCourseResults. Unable to parse " + url);
		}

		return results;
	}
	
	/**
	 * Adds the complete result table (including other runners) for the most recent run 
	 * that the athlete completed.
	 * 
	 * This is achieved by querying the db for the most recent run the runner has
	 * completed then taking the courseResultURL attribute from that record and passing
	 * it through to the getCourseResults method.
	 */
	public List<Result> getMostRecentRaceResults() {
		ArrayList<Result> mostRecentResults = new ArrayList<Result>();
		DatabaseHandler databaseHandler = new DatabaseHandler(context);
		
		for ( String athleteID : athleteIDs ) {
			String query = 
					"SELECT * FROM " + DatabaseHandler.TABLE_RESULTS + 
					" WHERE " + DatabaseHandler.KEY_RUNNER_ID + " = " + athleteID +
					" ORDER BY " + DatabaseHandler.KEY_COURSE_DATE + " DESC" +
					" LIMIT 1 ";
			mostRecentResults.addAll(databaseHandler.getResults(query));
		}
		databaseHandler.close();
		
		// now go off and concurrently fetch all of the most recent racers results tables
		Log.i(LOG_TAG, "Most recent results size=" + mostRecentResults.size());
		ExecutorService service = Executors.newFixedThreadPool(mostRecentResults.size());
		List<Future<List<Result>>> futureResults = new ArrayList<Future<List<Result>>>();
		
		for ( final Result r : mostRecentResults ) {
			Callable<List<Result>> callable = new Callable<List<Result>>() {
				public List<Result> call() {
					return getCourseResults(r.getCourseName(), r.getCourseNumber(), r.getCourseDate(), r.getCourseResultURL());
				}
			};
			futureResults.add(service.submit(callable));
		}
		service.shutdown();
		
		ArrayList<Result> mostRecentRaceResults = new ArrayList<Result>();
	    for (Future<List<Result>> future : futureResults) {
	    	try {
				mostRecentRaceResults.addAll(future.get());
			} catch (Exception e) {
				e.printStackTrace();
			}
	    }
		return mostRecentRaceResults;
	}

	/**
	 * Gets all runner results from a specific event. This function is used to allow the app to display
	 * other peoples results alongside the specified athletes.
	 * 
	 * Method works by connecting to the given url, downloading the html before scraping all 
	 * table data into result objects that can then be added to the database
	 * 
	 * @param courseName the name of the course as it appears on the parkrun website (e.g. 'highbury fields')
	 * @param courseNumber the number of the event. i.e. 124 = 124th event at the given course
	 * @param courseDate the date of the event. integer in the format YYYYMMDD
	 * @param url the url to the results page which needs to be downloaded
	 */
	public List<Result> getCourseResults(String courseName, int courseNumber, int courseDate, String url) {
		ArrayList<Result> results = new ArrayList<Result>();
		
		try {
			Log.i(LOG_TAG, "Attempting connection to event result page: " + url);
			Document doc = Jsoup.connect(url).get();
			
			// first get all the tables that the results could be in.
			Element resultTable = doc.getElementsByAttributeValueMatching("id", "results").get(0);
			
			// then add everything to a result object that can then be added into the database
			for (Element tr : resultTable.select("tr")) {
				Elements tds = tr.select("td");
				
				if (!tds.isEmpty()) {
					
					String rName = "";
					String[] rNames = tds.get(1).text().toLowerCase(Locale.US).split("\\s+");
					for ( String n : rNames ) {
						rName = (rName + " " + n.substring(0, 1).toUpperCase(Locale.US) + n.substring(1)).trim();
					}
					
					Result r;
					int rPosition = Integer.parseInt(tds.get(0).text()); 
					String cName = courseName;
					int cDate = courseDate;
					int cNumber = courseNumber;
					String cURL = url;
					
					if (rName.equals("Unknown")) {
						int rID = -1;
						int rTime = -1;
						String rAgeCat = "Unknown";
						String rAgeGrade = "Unknown";
						String rGender = "Unknown";
						r = new Result(rName, rID, rTime, rAgeCat, rPosition, rAgeGrade, rGender, cName, cDate, cNumber, cURL);
					} else {
						int rID = Integer.parseInt(tds.get(1).select("a").get(0).attr("href").split("athleteNumber=")[1]);
						int rTime = Integer.parseInt(((tds.get(2).text().split(":"))[0])+((tds.get(2).text().split(":"))[1]));
						String rAgeCat = tds.get(3).text();
						String rAgeGrade = tds.get(4).text().replaceAll("\\s+","");
						String rGender = tds.get(5).text();
						r = new Result(rName, rID, rTime, rAgeCat, rPosition, rAgeGrade, rGender, cName, cDate, cNumber, cURL);
					}
					results.add(r);
				}
			}		
		} catch (Exception e) {
			Log.e("Results","Error in getCourseResults. Unable to parse " + url);
			e.printStackTrace();
		}
		
		return results;
	}

}
