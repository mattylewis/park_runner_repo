package uk.co.mglewis.park_runner.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Handler class for the database which stores all run related data.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
     
	/**
	 * Database connections used by handler. 
	 * 
	 * Instantiated in constructor and closed on calling close() method.
	 */
	private SQLiteDatabase readableDatabase;
	private SQLiteDatabase writableDatabase;
	
	private final String LOG_TAG = "Park_Runner_DatabaseHandler";
	
    // Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "parkrun_results";
 
    // Table Names
    public static final String TABLE_RESULTS = "table_results";
 
    // Common column names
    private static final String KEY_ID = "id";
 
    // TABLE_RESULTS Table - column names
    public static final String KEY_RUNNER_NAME = "runner_name";
    public static final String KEY_RUNNER_ID = "runner_id";
    public static final String KEY_RUNNER_TIME = "runner_time";
    public static final String KEY_RUNNER_AGE_CAT = "runner_age_cat";
    public static final String KEY_RUNNER_POSITION = "runner_position";
    public static final String KEY_RUNNER_AGE_GRADE = "runner_age_grade";
    public static final String KEY_RUNNER_GENDER = "runner_gender";
    public static final String KEY_COURSE_NAME = "course_name";
    public static final String KEY_COURSE_DATE = "course_date";
    public static final String KEY_COURSE_NUMBER = "course_number";
    public static final String KEY_COURSE_RESULT_URL = "course_result_url";

    // Table Create Statement
    private static final String CREATE_TABLE_RESULTS = "CREATE TABLE " + TABLE_RESULTS + " ( " 
    		+ KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " 
    		+ KEY_RUNNER_NAME + " TEXT NOT NULL, "
    		+ KEY_RUNNER_ID + " INTEGER NOT NULL, "
    		+ KEY_RUNNER_TIME + " INTEGER NOT NULL, "
    		+ KEY_RUNNER_AGE_CAT + " TEXT NOT NULL, "
    		+ KEY_RUNNER_POSITION + " INTEGER NOT NULL, "
    		+ KEY_RUNNER_AGE_GRADE + " TEXT NOT NULL, "
    		+ KEY_RUNNER_GENDER + " TEXT NOT NULL, "
    		+ KEY_COURSE_NAME + " TEXT NOT NULL, "
    		+ KEY_COURSE_DATE + " INT NOT NULL, "
    		+ KEY_COURSE_NUMBER + " TEXT NOT NULL, "
    		+ KEY_COURSE_RESULT_URL + " TEXT "
    		+ ")";
    
    /**
     * Constructor for the database handler.
     * 
     * On creation of the class create the SQLLitDatase connection that all queries
     * can then use. This connection can be kept alive until the close method is called.
     */
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);     
        
        readableDatabase = this.getReadableDatabase();
        writableDatabase = this.getWritableDatabase();
    }
 
    /**
     * Creates the database. 
     * 
     * Currently the only table to be created is a the results table that contains
     * all runner results that have been downloaded
     * 
     * @param db the database to create
     */
    @Override
    public void onCreate(SQLiteDatabase db) {    	
        db.execSQL(CREATE_TABLE_RESULTS);
    }
    
    /**
     * On upgrade of database, drop all tables and create again. This will delete all data.
     * 
     * @param db the database to be upgraded
     * @param oldVersion the old version of the database
     * @param newVersion the new version of the database
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {        
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RESULTS);	
        onCreate(db);
    }
    
    /**
     * Adds a new result record to the database.
     * 
     * Logic is included in this method to ensure that duplicate records 
     * are not added to the database. This is achieved by calling the 
     * checkIfResultExistsInDB() method.
     * 
     * @param r the record to be added to the database
     */
    public void addResult(Result r) {
		if (checkIfResultExistsInDB(r) == false) {
			ContentValues values = new ContentValues();
		    values.put(KEY_RUNNER_ID,r.getRunnerID());
		    values.put(KEY_RUNNER_NAME,r.getRunnerName());
		    values.put(KEY_RUNNER_TIME,r.getRunnerTime());
			values.put(KEY_RUNNER_AGE_CAT,r.getRunnerAgeCat());
			values.put(KEY_RUNNER_POSITION,r.getRunnerPosition());
			values.put(KEY_RUNNER_AGE_GRADE,r.getRunnerAgeGrade());
			values.put(KEY_RUNNER_GENDER,r.getRunnerGender());
			values.put(KEY_COURSE_NAME,r.getCourseName());
			values.put(KEY_COURSE_DATE,r.getCourseDate());
			values.put(KEY_COURSE_NUMBER,r.getCourseNumber());
			values.put(KEY_COURSE_RESULT_URL,r.getCourseResultURL());
		
			try {
				writableDatabase.insert(TABLE_RESULTS, null, values);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    }
    
    /**
     * Adds a list of results into the database
     * 
     * @param results a list of results to add to the database
     */
    public void addResults(List<Result> results) {
    	for (Result r : results) {
    		addResult(r);
    	}
    }
    
    /**
     * Checks to see if result already exists in the database
     * 
     * @param r the result to check 
     * @return true if result already exists, false if no match found in the database
     * 
     * Possible to pass connection as argument from the add result class
     */
    public boolean checkIfResultExistsInDB(Result r) {
    	String query = "SELECT * FROM " + TABLE_RESULTS + " WHERE " 
    			+ KEY_COURSE_NAME + " = '" + r.getCourseName() + "' AND "
    			+ KEY_COURSE_DATE + " = " + r.getCourseDate() + " AND "
    			+ KEY_COURSE_NUMBER + " = " + r.getCourseNumber() + " AND "
    			+ KEY_RUNNER_ID + " = " + r.getRunnerID();
    	
        Cursor cursor = readableDatabase.rawQuery(query, null);
        int cursorCount = cursor.getCount();
        cursor.close();
        
        return ( cursorCount == 0) ? false : true;
    }
    
    /**
     * Returns an ArrayList of results from the database when given a select query
     * 
     * @param selectQuery the query to execute
     * @return an ArrayList of result objects
     */
    public ArrayList<Result> getResults(String selectQuery) {
    	ArrayList<Result> results = new ArrayList<Result>();    	
        Cursor c = readableDatabase.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
            	Result r = new Result(c.getString(1),c.getInt(2),c.getInt(3),c.getString(4),c.getInt(5),
            			c.getString(6),c.getString(7),c.getString(8),c.getInt(9),c.getInt(10),c.getString(11));
            	results.add(r);
            } while (c.moveToNext());
        }       
        return results;
    }
        
    /**
     * Returns an ArrayList of courses from the database when given a select query
     * 
     * @param selectQuery the query to execute
     * @return an ArrayList of course objects
     */
    public ArrayList<Course> getCourses(String selectQuery) {
    	ArrayList<Course> courses = new ArrayList<Course>();    	
        Cursor c = readableDatabase.rawQuery(selectQuery, null);
    	
        if (c.moveToFirst()) {
            do {
            	Course course = new Course(c.getString(0),c.getInt(1),c.getInt(2),c.getInt(3),c.getInt(4));
            	courses.add(course);
            } while (c.moveToNext());
        }               
        return courses;
    }
    
    /**
     * Returns an ArrayList of accounts from the database when given a set of account ids
     * 
     * @param accountIDs the accounts to fetch
     * @return an ArrayList of account objects
     */
    public ArrayList<Account> getAccounts(Set<String> accountIDs) {
    	ArrayList<Account> accounts = new ArrayList<Account>();
    	
    	for ( String accountID : accountIDs ) {
    		String query = " SELECT " +
    							" " + KEY_RUNNER_ID + ", " + 
    							" " + KEY_RUNNER_NAME + ", " + 
    							" COUNT( " + KEY_RUNNER_ID + " ) AS NUMBER_OF_RUNS, " +
    							" MAX( " + KEY_COURSE_DATE + " ) AS MOST_RECENT_RUN_DATE, " + 
    							" MIN( " + KEY_RUNNER_TIME + " ) AS BEST_TIME " +
							" FROM " + TABLE_RESULTS + 
							" WHERE " + KEY_RUNNER_ID + " = " + accountID + " ";
    		
    		Cursor c = readableDatabase.rawQuery(query , null);
    		
    		if (c.moveToFirst()) {
    			do {
    				Account a = new Account(c.getInt(0), c.getString(1), c.getInt(2), c.getInt(3), c.getInt(4));
    				accounts.add(a);
    			} while ( false );
    		}
    	}
    	
    	return accounts;
    }
    
    /**
     * Truncates the results table
     */
    public void truncate() {
        writableDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_RESULTS);	
        onCreate(writableDatabase);
    }
    
    /**
     * Closes the connection to the database
     */
    public void close() {
    	readableDatabase.close();
    	writableDatabase.close();
    }
}