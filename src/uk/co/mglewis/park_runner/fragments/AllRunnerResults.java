package uk.co.mglewis.park_runner.fragments;

import uk.co.mglewis.park_runner.R;
import uk.co.mglewis.park_runner.adapters.AllRunnerResultsAdapter;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * Page Fragment that is a child of the Screen Slide Activity
 * 
 * Class displays a result table for the all runs that the 
 * athlete has completed.
 */
public class AllRunnerResults extends Fragment {

	/**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "All Runner Results";

    /**
     * The fragment's page number, which is set to the argument value for ARG_PAGE.
     */
    private int mPageNumber;
    
    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     * 
     * @param pageNumber the page to create
     */
    public static AllRunnerResults create(int pageNumber) {
        AllRunnerResults fragment = new AllRunnerResults();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
    }
    
    /**
     * Inflates and populates the layout.  
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {        
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_slide_all_runner_results, container, false);
        
        ListView l = (ListView) rootView.findViewById(R.id.list_recent_results);
        AllRunnerResultsAdapter r = new AllRunnerResultsAdapter(this.getActivity());        
        l.setAdapter(r);
        
        return rootView;
    }

    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }
}
