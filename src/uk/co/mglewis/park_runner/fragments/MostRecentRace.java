package uk.co.mglewis.park_runner.fragments;

import uk.co.mglewis.park_runner.R;
import uk.co.mglewis.park_runner.adapters.MostRecentRaceAdapter;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Page Fragment that is a child of the Screen Slide Activity
 * 
 * Class displays the course result table for the run that the 
 * athlete most recently completed
 */
public class MostRecentRace extends Fragment {

	/**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "Most Recent Race Results";

    /**
     * The fragment's page number, which is set to the argument value for ARG_PAGE.
     */
    private int mPageNumber;
    
    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     * 
     * @param pageNumber the page to create
     */
    public static MostRecentRace create(int pageNumber) {
        MostRecentRace fragment = new MostRecentRace();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
    }

    /**
     * Inflates and populates the layout. 
     * 
     * Title of page is dynamically generated from data in the database
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_slide_most_recent_race, container, false);
        
        ListView l = (ListView) rootView.findViewById(R.id.list_recent_results);
        MostRecentRaceAdapter r = new MostRecentRaceAdapter(this.getActivity());
        l.setAdapter(r);      
        
        TextView title = (TextView)rootView.findViewById(R.id.title_most_recent_race);
        title.setText(r.getTitleText());
        
        return rootView;
    }

    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }
}
