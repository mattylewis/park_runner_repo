package uk.co.mglewis.park_runner.adapters;

import java.util.ArrayList;
import java.util.Set;

import uk.co.mglewis.park_runner.R;
import uk.co.mglewis.park_runner.data.Account;
import uk.co.mglewis.park_runner.data.DatabaseHandler;
 
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
 
/**
 * Adapter class that provides data to list of all results achieved by
 * a athlete
 */
public class LaunchpadAdaptor extends BaseAdapter {
 
    private ArrayList<Account> accountData;
    private static LayoutInflater inflater = null;

    /**
     * Constructor that:
     *  - inflates layout 
     *  - queries the database for all data required to populate the accounts list
     *  
     * @param a parent activity that adapter is created from
     */
    public LaunchpadAdaptor(Activity activity, Set<String> savedAccounts) {  
        DatabaseHandler dh = new DatabaseHandler(activity.getBaseContext());
        accountData = dh.getAccounts(savedAccounts);
        
        if ( accountData == null ) {
        	accountData = new ArrayList<Account>();
        }
        
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        dh.close();        
    }
    
    /**
     * Returns the number of saved accounts
     */
    public int getCount() {
        return accountData.size();
    }
 
    /**
     * Returns the position in the list. Required for implementing BaseAdaptor
     * 
     * This can be useful if implementing different formatting for odd/even rows.
     */
    public Object getItem(int position) {
        return position;
    }
 
    /**
     * Returns the position in the list. Required for implementing BaseAdaptor
     * 
     * This can be useful if implementing different formatting for odd/even rows.
     */
    public long getItemId(int position) {
        return position;
    }
     
    /**
     * @return the account ID of the given index of a saved account
     */
    public int getRunnerID(int position) {
    	return accountData.get(position).getAccountID();
    }
    
    /**
     * Populates a row in the list. Contents of the row is determined by the position variable.
     * 
     * @param position the position in the list, code uses this variable show the runner's nth run (P1 = most recent)
     * @param convertView the row view to update
     * @param parent the parent view that the convertView inherits from
     * @return the inflated and populated list row
     */
    public View getView(int position, View convertView, ViewGroup parent) {
        Account a = accountData.get(position);
        
        if ( a.getNumberOfRuns() != 0 )
        {
	        if (convertView == null ) {
	        	convertView = inflater.inflate(R.layout.list_row_saved_account, null);
	        }
	        
	        TextView aRunCount = (TextView)convertView.findViewById(R.id.pr_list_row_number_of_runs);
	        TextView aName = (TextView)convertView.findViewById(R.id.pr_list_row_name); 
	        TextView aBestTime = (TextView)convertView.findViewById(R.id.pr_list_row_pb); 
	        TextView aLastRun = (TextView)convertView.findViewById(R.id.pr_list_row_last_run);
	        
            aRunCount.setText("Runs " + a.getNumberOfRuns());
            aName.setText(a.getAccountName());
            aBestTime.setText("PB: " + a.getBestTimeAsString());
            aLastRun.setText("Last: " + a.getMostRecentRunDateAsString());        	
        }        
        return convertView;
    }
    
    /**
     * @return a view with loading text for a user who doesn't have data downloaded yet
     */
    public View getLoadingView()
    {
    	View view = inflater.inflate(R.layout.list_row_saved_account_loading, null);
    	return view;
    }
}