package uk.co.mglewis.park_runner.data;

import android.util.Log;

/** 
 * Container class for holding database data relating to course information
 */
public class Course {

	private final String LOG_TAG = "Park_Runner_Course";

	private String courseName;
    private int numberOfRuns;
    private int mostRecentRunDate;
    private int bestPosition;
    private int bestTime;
    
	public Course(String courseName, int numberOfRuns, int bestPosition, int bestTime, int mostRecentRunDate)
    {
    	this.courseName = courseName;
    	this.numberOfRuns = numberOfRuns;
    	this.bestPosition = bestPosition;
    	this.bestTime = bestTime;
    	this.mostRecentRunDate = mostRecentRunDate;
    }
    
    public String getCourseName() {
		return courseName;
	}

	public int getNumberOfRuns() {
		return numberOfRuns;
	}

	public int getMostRecentRunDate() {
		return mostRecentRunDate;
	}

	public String getMostRecentRunDateAsString() {
		String d = Integer.toString(mostRecentRunDate);
		String year = d.substring(0,4);
		String month = d.substring(4,6);
		String day = d.substring(6,8);
		
		return day + "/" + month + "/" + year;
	}
	
	public int getBestPosition() {
		return bestPosition;
	}

	public int getBestTime() {
		return bestTime;
	}
	
    public String getBestTimeAsString() {
    	String t = Integer.toString(bestTime);
    	String minutes = t.substring(0, t.length()-2); 
    	String seconds = t.substring(t.length()-2);
    	return minutes + ":" + seconds;
    }
    
	public void printResult() {
		Log.d(LOG_TAG,"------------------------------------------------");
		Log.d(LOG_TAG,"Course Name: " + courseName);
		Log.d(LOG_TAG,"Number Of Runs: " + numberOfRuns);
		Log.d(LOG_TAG,"Most Recent Run: " + mostRecentRunDate);
		Log.d(LOG_TAG,"Best Position: " + bestPosition);
		Log.d(LOG_TAG,"Best Time: " + bestTime);
		Log.d(LOG_TAG,"------------------------------------------------");
	}
	
}
