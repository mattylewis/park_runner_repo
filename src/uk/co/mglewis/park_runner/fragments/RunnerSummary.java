package uk.co.mglewis.park_runner.fragments;

import java.util.List;

import uk.co.mglewis.park_runner.R;
import uk.co.mglewis.park_runner.adapters.RunnerSummaryAdapter;
import uk.co.mglewis.park_runner.data.Course;
import uk.co.mglewis.park_runner.data.Result;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Page Fragment that is a child of the Screen Slide Activity
 * 
 * Class displays a variety of dash-board style information on 
 * runs that the athlete has completed
 */
public class RunnerSummary extends Fragment {
	
	private final String LOG_TAG = "Park_Runner_Runner_Summary";
	
    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "Runner Summary";
    
    /**
     * The fragment's page number, which is set to the argument value for ARG_PAGE.
     */
    private int mPageNumber;
    
    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     * 
     * @param pageNumber the page to create
     */
    public static RunnerSummary create(int pageNumber) {
        RunnerSummary fragment = new RunnerSummary();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        
        mPageNumber = getArguments().getInt(ARG_PAGE);
    }

    /**
     * Inflates and populates the layout. 
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_slide_runner_summary, container, false);
    
        RunnerSummaryAdapter runnerSummaryAdapter = new RunnerSummaryAdapter(getActivity());
        setupTextSummary(rootView, runnerSummaryAdapter);
        setupResultLists(rootView, runnerSummaryAdapter);
        setupCourseList(rootView, runnerSummaryAdapter);
        
        return rootView;
    }

    /**
     * Correctly sets up the text at the top of the summary page. 
     * 
     * Text should be set as follows:
     * "23 parkruns completed. Last run 6 days ago"
     * 
     * @param rootView summary page
     * @param runnerSummaryAdapter the adapter for pulling data from the database
     */
    private void setupTextSummary(ViewGroup rootView, RunnerSummaryAdapter runnerSummaryAdapter) {
    	TextView textSummary = (TextView)rootView.findViewById(R.id.text_runner_summary);
    	textSummary.setText(runnerSummaryAdapter.getNumberOfRunsAsString() + " " + runnerSummaryAdapter.getDaysSinceLastRunAsString());
    }
    
    /**
     * Populates the recent results and best results cards on the summary screen
     * 
     * @param rootView summary page
     * @param runnerSummaryAdapter the adapter for pulling data from the database
     */
    private void setupResultLists(ViewGroup rootView, RunnerSummaryAdapter runnerSummaryAdapter) {

    	// recent times
    	List<Result> recentTimes = runnerSummaryAdapter.getRecentTimes();
    	setupResultRow(rootView.findViewById(R.id.summary_row_recent_result_1),recentTimes,0);
    	setupResultRow(rootView.findViewById(R.id.summary_row_recent_result_2),recentTimes,1);
    	setupResultRow(rootView.findViewById(R.id.summary_row_recent_result_3),recentTimes,2);

    	// best times
    	List<Result> bestTimes = runnerSummaryAdapter.getBestTimes();
    	setupResultRow(rootView.findViewById(R.id.summary_row_best_result_1),bestTimes,0);
    	setupResultRow(rootView.findViewById(R.id.summary_row_best_result_2),bestTimes,1);
    	setupResultRow(rootView.findViewById(R.id.summary_row_best_result_3),bestTimes,2);
    }   
    
    /**
     * Populates a single results row on the summary screen (or hides the view if no data)
     * 
     * @param result the row layout object
     * @param results the list of results to use
     * @param index the index in the list of the results to use
     */
    private void setupResultRow(View result, List<Result> results, int index) {
    	if ( results.size() > index ) {
    		TextView time = (TextView)result.findViewById(R.id.pr_list_row_time);
            TextView course = (TextView)result.findViewById(R.id.pr_list_row_course);
            TextView position = (TextView)result.findViewById(R.id.pr_list_row_position);
            TextView date = (TextView)result.findViewById(R.id.pr_list_row_date);
            Result r = results.get(index);
            time.setText(r.getRunnerTimeAsString());
            course.setText(r.getCourseName());
            position.setText("Position: " + r.getRunnerPositionAsString());
            date.setText("Date: " + r.getCourseDateAsString());
    	} else {
    		((LinearLayout)result.getParent()).removeView(result);
    	}
    }
        
    /**
     * Populates the favourite course card on the summary screen
     * 
     * @param rootView summary page
     * @param runnerSummaryAdapter the adapter for pulling data from the database
     */
    private void setupCourseList(ViewGroup rootView, RunnerSummaryAdapter runnerSummaryAdapter) {
    	List<Course> favouriteCourses = runnerSummaryAdapter.getFavouriteCourses();
    	setupCourseRow(rootView.findViewById(R.id.summary_row_favourite_course_1),favouriteCourses,0);
    	setupCourseRow(rootView.findViewById(R.id.summary_row_favourite_course_2),favouriteCourses,1);
    	setupCourseRow(rootView.findViewById(R.id.summary_row_favourite_course_3),favouriteCourses,2);
    }
    
    /**
     * Populates a single course row on the summary screen (or hides the view if no data)
     * 
     * @param course the row layout object
     * @param courses the list of courses to use
     * @param index the index in the list of the courses to use
     */
    private void setupCourseRow(View course, List<Course> courses, int index) {
    	if ( courses.size() > index ) {
    		TextView courseName = (TextView)course.findViewById(R.id.pr_list_row_course);
    		TextView numberOfRuns = (TextView)course.findViewById(R.id.pr_list_row_number_of_runs);
    		TextView personalBest = (TextView)course.findViewById(R.id.pr_list_row_best_time);
            TextView mostRecentRace = (TextView)course.findViewById(R.id.pr_list_row_last_run);
            Course c = courses.get(index);
            courseName.setText(c.getCourseName());
            
            String suffix;
            if ( c.getNumberOfRuns() == 1 ) {
            	suffix = " run";
            } else {
            	suffix = " runs";
            }
            
            numberOfRuns.setText(c.getNumberOfRuns() + suffix);
            personalBest.setText("PB: " + c.getBestTimeAsString());
            mostRecentRace.setText("Last: " + c.getMostRecentRunDateAsString());
    	} else {
    		((LinearLayout)course.getParent()).removeView(course);
    	}
    }
    
    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }
}
